#pragma once
#include <algorithm>
#include <cmath>
#include <numeric>

template <typename T>
inline T sigma(T x)
{
    return static_cast<T>(1.0) / (1.0 + exp(-x));
}

template <typename T>
inline T sigma_der(T x)
{
    const auto sx = sigma(x);
    return sx * (1.0 - sx);
}

template <typename T, template <typename> class V0,
          template <typename> class V1>
inline T dot(const V0<T>& a, const V1<T>& b)
{
    const auto s = std::min(a.size(), b.size());
    T ret = 0;
    for (size_t i = 0; i < s; ++i)
        ret += a[i] * b[i];

    return ret;
}

template <typename T, template <typename> class V0,
          template <typename> class V1>
inline T dot_bias(const V0<T>& a, const V1<T>& b, T bias)
{
    return dot(a, b) + bias;
}

template <typename T, template <typename> class V0>
inline T sum(const V0<T>& v)
{
    return std::accumulate(v.begin(), v.end(), static_cast<T>(0));
}

template <typename T, template <typename> class V>
inline size_t get_max_index(const V<T>& v)
{
    const auto begin_it = v.begin();
    const auto it = std::max_element(begin_it, v.end());
    return std::distance(begin_it, it);
}

template <typename T, typename A, template <typename, typename> class V>
inline size_t get_max_index(const V<T, A>& v)
{
    const auto begin_it = v.begin();
    const auto it = std::max_element(begin_it, v.end());
    return std::distance(begin_it, it);
}

template <typename W>
struct ActivationExp {
    static W sigma(W in) { return ::sigma(in); }
    static W sigma_der(W in) { return ::sigma_der(in); }
};

template <typename W>
struct ActivationTanh {
    static W sigma(W in) { return std::tanh(in); }
    static W sigma_der(W in)
    {
        const auto t = std::tanh(in);
        return 1 - t * t;
    }
};
