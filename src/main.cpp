#include "utils/utils.hpp"
#include <fstream>
#include <vector>
#include <vvvstdhelper/containerhelper.hpp>
#include <vvvstdhelper/random.hpp>
#include <vvvstdhelper/time.hpp>
#include <vvvstdhelper/view.hpp>

#include "mnist/mnist.hpp"
#include "nn/nnet.hpp"

template <typename T = double>
class Perceptron {
public:
    using weight_t = T;
    using error_t = weight_t;
    using View = vvv::VarView<T>;
    using ConstView = vvv::View<T>;

    Perceptron(T* start, size_t num_inputs) : weights(start, num_inputs)
    {
        for (auto& w : weights)
            w = vvv::random(-1.0f, 1.0f);
    }
    Perceptron(std::vector<T>& data) : Perceptron(data.data(), data.size()) {}

    weight_t feed(const ConstView& inputs) const
    {
        return sigma(dot_bias(inputs, weights, bias));
    }

    error_t train(const ConstView& inputs, weight_t expected,
                  weight_t learning_speed = 0.1)
    {
        const auto res = feed(inputs);
        const auto err = expected - res;
        const auto k = learning_speed * err * sigma_der(res);
        const auto s = weights.size();
        for (size_t i = 0; i < s; ++i)
            weights[i] += inputs[i] * k;
        bias += k;
        return err;
    }

private:
    View weights;
    weight_t bias = 1.0;
};

template <typename T, typename P>
std::vector<T> gen_inputs(size_t count, const P& p)
{
    std::vector<T> ret;
    ret.reserve(count);

    for (size_t i = 0; i < count; ++i)
        ret.push_back(p());

    return ret;
}
template <typename T>
std::vector<T> gen_inputs(size_t count, T min, T max)
{
    return gen_inputs<T>(count, [min, max]() { return vvv::random(min, max); });
}

template <typename T>
struct Sample {
    std::vector<T> inputs;
    T expected_result;
};

/// P function calculates correct result
template <typename T, typename F, typename P>
std::vector<Sample<T>> gen_data_set(size_t set_size, size_t inputs_count,
                                    const F& generate, const P& p_result)
{
    std::vector<Sample<T>> ret;
    ret.reserve(set_size);

    for (size_t i = 0; i < set_size; ++i) {
        Sample<T> sample;
        sample.inputs = gen_inputs<T>(inputs_count, generate);
        sample.expected_result = p_result(sample.inputs);
        ret.push_back(std::move(sample));
    }

    return ret;
}

template <typename Gen, typename Exp>
void learn_and_test(size_t num_inputs, size_t epoch_count,
                    double learning_speed, size_t train_size, size_t test_size,
                    const Gen& gen, Exp& func_expected)
{
    const auto train_data =
        gen_data_set<double>(train_size, num_inputs, gen, func_expected);
    const auto test_data =
        gen_data_set<double>(test_size, num_inputs, gen, func_expected);

    std::cout << "create neuron...\n";

    std::vector<double> weights(num_inputs);
    Perceptron<> n(weights);
    std::cout << "train neuron...\n";
    double err = 0;
    for (size_t e = 0; e < epoch_count; ++e) {
        for (const auto& sample : train_data) {
            err =
                n.train(sample.inputs, sample.expected_result, learning_speed);
        }
    }
    std::cout << "err = " << err << "\n";

    std::cout << "test neuron...\n";
    size_t count_positive = 0;
    for (const auto& sample : test_data) {
        const auto& result = n.feed(sample.inputs);
        const auto err = sample.expected_result - result;
        if (std::fabs(err) < 0.5)
            ++count_positive;
    }
    const size_t results = test_data.size();

    std::cout << "got " << count_positive << " correct results from " << results
              << ", " << (static_cast<double>(count_positive) / results * 100)
              << "%"
              << "\n";
}

void neuron_and()
{
    const auto num_inputs = 2;
    const auto epoch_count = 40;
    const auto learning_speed = 0.1;
    const auto train_size = 100;
    const auto test_size = 100;
    const auto func = [](const auto& v) {
        int x0 = std::round(v[0]);
        int x1 = std::round(v[1]);
        return x0 & x1;
    };
    const auto gen = [] { return static_cast<double>(vvv::random(0, 1)); };

    learn_and_test(num_inputs, epoch_count, learning_speed, train_size,
                   test_size, gen, func);
}

void majoritary()
{
    const auto num_inputs = 20;
    const auto epoch_count = 4000;
    const auto learning_speed = 0.1;
    const auto train_size = 1000;
    const auto test_size = 200;
    const auto func = [](const auto& vs) {
        const auto s = sum(vvv::make_view(vs));
        return s > num_inputs / 2;
    };
    const auto gen = [] { return static_cast<double>(vvv::random(0, 1)); };

    learn_and_test(num_inputs, epoch_count, learning_speed, train_size,
                   test_size, gen, func);
}

template <typename T>
class PercentileMeter {
public:
    void clear() { data.clear(); }
    void add(const T& t) { data.push_back(t); }
    void add(T&& t) { data.push_back(std::move(t)); }
    T getPercentile(float percent) const
    {
        if (data.empty())
            return 0;

        sort();
        const auto frac = std::min(100.0f, percent) / 100;
        size_t pos = std::max<size_t>(data.size() * frac, data.size() - 1);
        return data[pos];
    }

private:
    void sort() const
    {
        if (sorted)
            return;

        std::sort(data.begin(), data.end());
        sorted = true;
    }

    mutable std::vector<T> data;
    mutable bool sorted = false;
};

template <typename W, typename Gen, typename Exp>
void nn_learn_and_test(const std::vector<size_t> num_inputs, size_t epoch_count,
                       W learning_speed, size_t train_size, size_t test_size,
                       const Gen& gen, Exp& func_expected)
{
    const auto train_data =
        gen_data_set<W>(train_size, num_inputs[0], gen, func_expected);
    const auto test_data =
        gen_data_set<W>(test_size, num_inputs[0], gen, func_expected);

    std::cout << "create neuron...\n";

    NNet<W> n(num_inputs);
    std::cout << "train neuron...\n";
    W err = 0;
    for (size_t e = 0; e < epoch_count; ++e) {
        for (const auto& sample : train_data) {
            err =
                n.train(sample.inputs,
                        {&sample.expected_result, &sample.expected_result + 1},
                        learning_speed)[0];
        }
    }
    std::cout << "err = " << err << "\n";

    std::cout << "test neuron...\n";
    size_t count_positive = 0;
    for (const auto& sample : test_data) {
        const auto& result = n.feed(sample.inputs)[0];
        const auto err = sample.expected_result - result;
        if (std::fabs(err) < 0.5)
            ++count_positive;
    }
    const size_t results = test_data.size();

    std::cout << "got " << count_positive << " correct results from " << results
              << ", " << (static_cast<double>(count_positive) / results * 100)
              << "%"
              << "\n";
}

void majoritarynn()
{
    const auto num_inputs = 20;
    const auto epoch_count = 4000;
    const auto learning_speed = 0.1;
    const auto train_size = 1000;
    const auto test_size = 200;
    const auto func = [](const auto& vs) {
        const auto s = sum(vvv::make_view(vs));
        return s > num_inputs / 2;
    };
    const auto gen = [] { return static_cast<double>(vvv::random(0, 1)); };

    nn_learn_and_test({num_inputs, 1}, epoch_count, learning_speed, train_size,
                      test_size, gen, func);
}
void nn_and()
{
    const auto num_inputs = 2;
    const auto epoch_count = 100;
    const auto learning_speed = 0.1;
    const auto train_size = 100;
    const auto test_size = 100;
    const auto func = [](const auto& v) {
        const int x0 = std::round(v[0]);
        const int x1 = std::round(v[1]);
        return x0 & x1;
    };
    const auto gen = [] { return static_cast<double>(vvv::random(0, 1)); };

    nn_learn_and_test({num_inputs, 1}, epoch_count, learning_speed, train_size,
                      test_size, gen, func);
}

void nn_xor()
{
    const auto num_inputs = 2;
    const auto epoch_count = 5000;
    const auto learning_speed = 0.3;
    const auto train_size = 100;
    const auto test_size = 200;
    const auto func = [](const auto& v) {
        const int x0 = std::round(v[0]);
        const int x1 = std::round(v[1]);
        return x0 ^ x1;
    };
    const auto gen = [] { return static_cast<double>(vvv::random(0, 1)); };

    nn_learn_and_test({num_inputs, 4, 1}, epoch_count, learning_speed,
                      train_size, test_size, gen, func);
}

namespace {

template <typename T = double>
struct Sample {
    using value_type = T;
    std::vector<T> inputs;
    std::vector<T> result;
};

template <typename T = double>
struct Set {
    std::vector<Sample<T>> samples;
};

template <typename T = double>
Set<T> load_set(const std::string& images_filename,
                const std::string& labels_filename)
{
    Set<T> ret;
    const auto& images = readImages(images_filename);
    const auto& labels = readLabels(labels_filename);
    ret.samples.resize(images.size());
    vvv::helpers::map(
        ret.samples,
        [](const mnist_image& image, const uint8_t label) {
            Sample<T> ret;
            ret.inputs = vvv::helpers::map(
                [](const uint8_t p) { return p * (static_cast<T>(1.0) / 255); },
                image.data);
            ret.result.resize(10);
            ret.result.at(label) = 1.0f;
            return ret;
        },
        images, labels);

    return ret;
}

/// Return view on output errors
template <typename T>
typename NNet<T>::ConstView nn_train(NNet<T>& nn, const Set<T>& set,
                                     size_t epoch_count, T learning_speed)
{
    for (size_t e = 0; e < epoch_count; ++e) {
        // std::cout << "---: " << nn.getWeights() << "\n";
        for (const auto& s : set.samples) {
            nn.train(s.inputs, s.result, learning_speed);
        }
        // std::cout << "+++: " << nn.getWeights() << "\n";
        std::cout << "epoch: " << e << "\n";
        std::cout << "errors: " << nn.getOutputErrors() << "\n";
    }
    return nn.getOutputErrors();
}

template <typename T>
size_t nn_check_classifier(NNet<T>& nn, const Set<T>& set)
{
    size_t correct_result_count = 0;
    for (const auto& s : set.samples) {
        const auto& result_index = nn.classify(s.inputs);
        const auto& expected_index = get_max_index(s.result);
        correct_result_count += (result_index == expected_index);
    }

    return correct_result_count;
}

} // namespace

template <typename W = float>
void mnist()
{
    const auto training_set = load_set<W>("./data/train-images-idx3-ubyte",
                                          "./data/train-labels-idx1-ubyte");
    const auto control_set = load_set<W>("./data/t10k-images-idx3-ubyte",
                                         "./data/t10k-labels-idx1-ubyte");
    const auto input_layer_size = training_set.samples.front().inputs.size();
    const auto output_layer_size = training_set.samples.front().result.size();
    NNet<W> nn({input_layer_size, 300, output_layer_size});
    nn_train(nn, training_set, 10, static_cast<W>(0.1));
    const float correct = nn_check_classifier(nn, control_set);
    const auto correct_percent = correct / control_set.samples.size();
    std::cout << "correct answers: " << (correct_percent * 100) << "%\n";

    std::ofstream f("mnist.nn");
    nn_write<W>(f, nn);
};

template <typename W>
NNet<W> nn_create(const std::vector<size_t>& hidden_layers = {300})
{
    std::ifstream f("./data/train-images-idx3-ubyte");
    mnist_image_header header;
    binaryRead(f, header);
    const size_t inputs = header.columns * header.rows;
    const size_t outputs = 10;
    std::vector<size_t> layers;
    layers.push_back(inputs);
    layers = layers + hidden_layers;
    layers.push_back(outputs);
    return NNet<W>(layers);
}

template <typename W>
void train(NNet<W>& nn, size_t epochs = 1, W learning_rate = 0.1)
{
    const auto training_set = load_set<W>("./data/train-images-idx3-ubyte",
                                          "./data/train-labels-idx1-ubyte");
    nn_train(nn, training_set, epochs, learning_rate);
}

template <typename W>
void check(NNet<W>& nn);

template <typename W>
void create_and_save(const std::vector<size_t>& hidden_layers,
                     W learning_rate = 0.1)
{
    const size_t epoch_count = 1;
    auto nn = nn_create<W>(hidden_layers);
    train(nn, epoch_count, learning_rate);
    check(nn);
    std::ofstream f("mnist.nn");
    nn_write<W>(f, nn);
}

template <typename W>
void print_percentiles(vvv::VarView<W> v)
{
    std::sort(v.begin(), v.end());
    for (const auto& p :
         {50.0, 70.0, 80.0, 90.0, 95.0, 98.0, 99.0, 99.5, 99.8}) {
        const size_t position = v.size() * p / 100.0;
        std::cout << "p" << p << "%: " << v[position] << "\n";
    }
}

template <typename W>
void check(NNet<W>& nn)
{
    const auto control_set = load_set<W>("./data/t10k-images-idx3-ubyte",
                                         "./data/t10k-labels-idx1-ubyte");
    auto t0 = vvv::now();
    const float correct = nn_check_classifier(nn, control_set);
    auto t1 = vvv::now();
    const auto correct_percent = correct / control_set.samples.size();
    std::cout << "correct answers: " << (correct_percent * 100) << "%\n";
    vvv::print_dt(std::cout, t0, t1);
    std::cout << "\n";

    // auto ws = nn.getWeights();
    // print_percentiles(ws);
}

template <typename W>
void load_and_check()
{
    auto nn = nn_read<W>("mnist.nn");
    check(nn);
}

template <typename W>
void load_train_check(int argc, char** argv, W learning_rate = 0.1)
{
    size_t epoch_count = 1;
    if (argc > 1) {
        const auto epoch_count_arg = argv[1];
        auto c = std::stoul(epoch_count_arg);
        if (c > 0)
            epoch_count = c;
    }

    auto nn = nn_read<W>("mnist.nn");
    train(nn, epoch_count, learning_rate);
    auto ws = nn.getWeights();
    std::vector<W> old(ws.begin(), ws.end());
    check(nn);

    std::ofstream f("mnist.nn");
    nn_write<W>(f, nn);
    f.close();
}

int main(int argc, char** argv)
{
    using W = float;
    const W learning_rate = 0.5;
    // test_read_write<W>();
    // create_and_save<W>({300, 64}, learning_rate);
    load_train_check<W>(argc, argv, learning_rate);
    // majoritarynn();
    // nn_and();
    // nn_xor();

    // mnist<double>();
    // mnist<float>();
}
