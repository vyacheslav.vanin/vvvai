#pragma once
#include <arpa/inet.h>
#include <fstream>
#include <iostream>
#include <vector>

struct mnist_label_header {
    int32_t magic;
    int32_t count;
};

struct mnist_image_header {
    int32_t magic;
    int32_t images_count;
    int32_t rows;
    int32_t columns;
};

struct mnist_image {
    using pixel = uint8_t;
    std::vector<pixel> data;
};

template <typename T>
inline std::istream& binaryRead(std::istream& str, T& v)
{
    str.read(reinterpret_cast<char*>(&v), sizeof(v));
    return str;
}

/// Read vector of type \p T of size \p size
template <typename T>
inline std::istream& binaryRead(std::istream& str, std::vector<T>& v,
                                size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        T elem;
        str.read(reinterpret_cast<char*>(&elem), sizeof(elem));
        v.push_back(elem);
    }
    return str;
}

template <>
inline std::istream& binaryRead(std::istream& str, mnist_label_header& header)
{
    int magic;
    int count;
    binaryRead(str, magic);
    binaryRead(str, count);
    magic = ntohl(magic);
    if (magic != 2049)
        throw std::logic_error("invalid magic");

    count = ntohl(count);
    if (count < 0)
        throw std::logic_error("invalid count");

    header.magic = magic;
    header.count = count;

    return str;
}

template <>
inline std::istream& binaryRead(std::istream& str, mnist_image_header& header)
{
    int magic;
    int count;
    int rows;
    int columns;
    binaryRead(str, magic);
    binaryRead(str, count);
    binaryRead(str, rows);
    binaryRead(str, columns);

    magic = ntohl(magic);
    if (magic != 2051)
        throw std::logic_error("invalid magic");

    count = ntohl(count);
    if (count < 0)
        throw std::logic_error("invalid count");

    rows = ntohl(rows);
    if (rows < 1)
        throw std::logic_error("invalid rows");

    columns = ntohl(columns);
    if (columns < 1)
        throw std::logic_error("invalid columns");

    header.magic = magic;
    header.images_count = count;
    header.rows = rows;
    header.columns = columns;

    return str;
}

inline std::ostream& operator<<(std::ostream& str,
                                const mnist_label_header& header)
{
    str << "[label header: " << header.magic << ", " << header.count << "]";
    return str;
}
inline std::ostream& operator<<(std::ostream& str,
                                const mnist_image_header& header)
{
    str << "[images header: " << header.magic << ", " << header.images_count
        << ", " << header.rows << ", " << header.columns << "]";
    return str;
}

/// return vector of labels. Each label contain number [0-9]
inline std::vector<uint8_t> readLabels(const std::string& filename)
{
    std::ifstream labels(filename);
    mnist_label_header header;
    binaryRead(labels, header);
    std::cout << "!!!! " << header << "\n";

    std::vector<uint8_t> ret;
    ret.reserve(header.count);
    for (int32_t i = 0; i < header.count; ++i) {
        uint8_t l;
        binaryRead(labels, l);
        ret.push_back(l);
    }
    return ret;
}

inline std::vector<mnist_image> readImages(const std::string& filename)
{
    std::ifstream file(filename);
    mnist_image_header header;
    binaryRead(file, header);
    std::cout << "!!!! im: " << header << "\n";
    const auto images_count = header.images_count;
    const auto data_size = header.columns * header.rows;

    std::vector<mnist_image> ret;
    ret.reserve(images_count);
    for (int i = 0; i < images_count; ++i) {
        mnist_image elem;
        binaryRead(file, elem.data, data_size);
        ret.push_back(std::move(elem));
    }

    return ret;
}
