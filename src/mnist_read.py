#! /usr/bin/env python3
import struct


def print_labels_header(filename):
    with open(filename, 'rb') as f:
        header = f.read(8)
        magic, item_count = struct.unpack('!ii', header)
        if magic != 0x801:
            raise Exception('This is not a mnist label file')

        print(f'{filename} contains magic={magic}, item_count={item_count}')


def read_labels(filename):
    with open(filename, 'rb') as f:
        header = f.read(8)
        magic, item_count = struct.unpack('!ii', header)
        if magic != 0x801:
            raise Exception(f'{filename} is not a mnist label file')

        for i in range(item_count):
            yield (i, f.read(1)[0])


def print_test_set_header(filename):
    with open(filename, 'rb') as f:
        header = f.read(16)
        magic, image_count, rows, columns = struct.unpack('!4i', header)
        if magic != 0x803:
            raise Exception(f'{filename} is not a mnist label file')

        print(f'{filename} contains magic={magic}, '
              f'image_count={image_count}, rows={rows}, columns={columns}')


def read_images(filename):
    with open(filename, 'rb') as f:
        header = f.read(16)
        magic, image_count, rows, columns = struct.unpack('!4i', header)
        if magic != 0x803:
            raise Exception(f'{filename} is not a mnist label file')

        for _ in range(image_count):
            image = []
            for _ in range(rows * columns):
                image.append(f.read(1)[0])
            yield image


def main():
    # for i in read_labels('./data/t10k-labels-idx1-ubyte'):
    #     print(i)
    print_test_set_header('./data/t10k-images-idx3-ubyte')
    print_test_set_header('./data/train-images-idx3-ubyte')
    for i in read_images('./data/t10k-images-idx3-ubyte'):
        print(i)


if __name__ == '__main__':
    main()
