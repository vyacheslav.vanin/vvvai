#include "nnet.hpp"
#include <vvvstdhelper/containerhelper.hpp>
#include <vvvstdhelper/endian.hpp>

#include <arpa/inet.h>
#include <iostream>

namespace {
size_t sum(vvv::View<size_t> v)
{
    return std::accumulate(v.begin(), v.end(), 0);
}

size_t calculate_weights_count(vvv::View<size_t> v)
{
    if (v.size() == 0)
        return 0;

    const auto l = vvv::slice(v, 0, v.size() - 1);
    const auto r = vvv::slice(v, 1, v.size());
    return dot(l, r);
}

using vvv::make_view;
using vvv::slice;
// using LayerDesc = NNet::LayerDesc;
// using W = NNet::W;

//   0 1 2 3   layer
// 5 4 3 1     neurons on each layer
// 0000011112223
// iiiii       o
//  0 layer
//  0 5
//
//  1 layer
//  5 9
//
//  2 layer
//  9 12
//
//  3 layer
//  12

// layer count starts with first non-input layer [0, N]
template <typename W>
vvv::VarView<W> make_inputs(size_t layer, const vvv::View<size_t>& layer_desc,
                            std::vector<W>& inputs)
{
    using vvv::slice;
    const auto& prev_layer = slice(layer_desc, 0, layer);
    const auto start_idx = sum(prev_layer);
    const auto start = inputs.data() + start_idx;
    const auto end = start + layer_desc[layer];

    return {start, end};
}

template <typename W>
W& make_output(size_t layer, size_t pos_in_layer,
               const vvv::View<size_t>& layer_desc, std::vector<W>& inputs)
{
    /// TODO: reuse
    auto inputs_view = make_inputs(layer, layer_desc, inputs);
    return *(inputs_view.end() + pos_in_layer);
}

template <typename W>
vvv::VarView<W> make_weights(size_t layer, size_t pos_in_layer,
                             const vvv::View<size_t>& layer_desc,
                             std::vector<W>& weights)
{
    const auto start_idx =
        calculate_weights_count(slice(layer_desc, 0, layer)) +
        pos_in_layer * layer_desc[layer];
    const auto start = weights.data() + start_idx;
    const auto end = start + layer_desc[layer];

    return {start, end};
}

template <typename W>
Neuron<W> make_neuron(size_t layer, size_t pos_in_layer,
                      const typename NNet<W>::LayerDesc& layer_desc,
                      std::vector<W>& inputs, std::vector<W>& output_dots,
                      std::vector<W>& weights, std::vector<W>& errors)
{
    const auto layer_desc_view = vvv::make_view(layer_desc);
    auto inputs_view = make_inputs(layer, layer_desc_view, inputs);
    auto weights_view =
        make_weights(layer, pos_in_layer, layer_desc_view, weights);
    auto& output = make_output(layer, pos_in_layer, layer_desc_view, inputs);
    auto& output_dot =
        make_output(layer, pos_in_layer, layer_desc_view, output_dots);
    auto output_errors = make_inputs(layer, layer_desc, errors);
    auto& input_error =
        make_output(layer, pos_in_layer, layer_desc_view, errors);
    return Neuron<W>(weights_view, inputs_view, output, output_dot, input_error,
                     output_errors);
}
} // namespace

template <typename W>
NNet<W>::NNet(const LayerDesc& ns)
    : neurons_by_layers(ns), neurons(), inputs(sum(make_view(ns))),
      input_dots(inputs.size()), weights(calculate_weights_count(ns), 1.0),
      errors(inputs.size()),
      input_layer(vvv::slice(vvv::make_view(inputs), 0, ns[0])),
      output_dots(vvv::slice(vvv::make_view(input_dots),
                             input_dots.size() - ns.back(), input_dots.size())),
      output_layer(vvv::slice(vvv::make_view(inputs), inputs.size() - ns.back(),
                              inputs.size())),
      output_errors(vvv::slice(vvv::make_view(errors),
                               errors.size() - ns.back(), errors.size()))
{
    W w_init = static_cast<W>(1.0) / weights.size();
    std::fill(weights.begin(), weights.end(), w_init);
    const auto neurons_layers = slice(make_view(ns), 1, ns.size());
    for (size_t l = 0; l < neurons_layers.size(); ++l) {
        for (size_t i = 0; i < neurons_layers[l]; ++i) {
            neurons.push_back(
                make_neuron(l, i, ns, inputs, input_dots, weights, errors));
        }
    }
}

template <typename W>
NNet<W>::NNet(size_t inputs) : NNet({inputs, 1})
{
}

template <typename W>
typename NNet<W>::ConstView NNet<W>::feed(ConstView inputs)
{
    std::copy(inputs.begin(), inputs.end(), getInputLayer().begin());
    for (auto& n : neurons)
        n.feed();

    return getOutputLayer();
}

template <typename W>
size_t NNet<W>::classify(ConstView inputs)
{
    return get_max_index(feed(inputs));
}

template <typename W>
typename NNet<W>::ConstView
NNet<W>::train(ConstView inputs, ConstView expected_outputs, W learning_speed)
{
    using vvv::helpers::map;

    std::fill(errors.begin(), errors.end(), 0);

    auto result = feed(inputs);
    assert(result.size() == expected_outputs.size());

    map(
        output_errors,
        [](const auto& expected, const auto& result, const auto& dots) {
            return Neuron<W>::sigma_der(dots) * (expected - result);
        },
        expected_outputs, result, output_dots);

    std::for_each(neurons.rbegin(), neurons.rend(),
                  [](Neuron<W>& n) { n.distribute_error(); });
    for (auto& n : neurons)
        n.update_weights(learning_speed);

    // in reverse order call update weights and distribute errors

    return output_errors;
}

namespace {
template <typename T>
std::ostream& binaryWrite(std::ostream& str, const T& t)
{
    str.write(reinterpret_cast<const char*>(&t), sizeof(t));
    return str;
}

template <typename T>
std::ostream& binaryWriteNet(std::ostream& str, const T& t)
{
    const auto be = vvv::hton(t); // to big endian
    return binaryWrite(str, be);
}

template <typename T>
std::istream& binaryRead(std::istream& str, T& t)
{
    str.read(reinterpret_cast<char*>(&t), sizeof(t));
    return str;
}

template <typename T>
std::istream& binaryReadNet(std::istream& str, T& t)
{
    binaryRead(str, t);
    t = vvv::ntoh(t);
    return str;
}
} // namespace

template class NNet<float>;
template class NNet<double>;

const uint64_t vvvai_magic = 0x555A1;
template <typename W>
void nn_write(std::ostream& str, const NNet<W>& nn)
{
    const auto& layers = nn.getLayerDesc();
    const size_t layer_count = layers.size();
    binaryWriteNet(str, vvvai_magic);
    binaryWriteNet(str, layer_count);
    for (const size_t layer : layers)
        binaryWriteNet(str, layer);

    const auto& weights = nn.getWeights();
    for (const W w : weights)
        binaryWriteNet(str, w);
}

template <typename W>
NNet<W> nn_read(std::istream& str)
{
    uint64_t magic = 0;
    size_t layers_count = 0;
    typename NNet<W>::LayerDesc layers;
    binaryReadNet(str, magic);
    if (magic != vvvai_magic)
        throw std::runtime_error("Invalid magic");

    binaryReadNet(str, layers_count);
    layers.resize(layers_count);
    for (size_t& layer : layers)
        binaryReadNet(str, layer);

    NNet<W> ret(layers);
    auto ws = ret.getWeights();
    for (W& w : ws)
        binaryReadNet(str, w);

    return ret;
}

template void nn_write<float>(std::ostream& str, const NNet<float>& nn);

template void nn_write<double>(std::ostream& str, const NNet<double>& nn);

template NNet<float> nn_read<float>(std::istream& str);

template NNet<double> nn_read<double>(std::istream& str);
