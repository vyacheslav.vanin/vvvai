#pragma once
#include "utils/utils.hpp"
#include <algorithm>
#include <numeric>
#include <vector>
#include <vvv3d/vvvmath/linalg.hpp>
#include <vvvstdhelper/random.hpp>
#include <vvvstdhelper/view.hpp>

template <typename W = float>
class Neuron {
public:
    using View = vvv::VarView<W>;
    using ConstView = vvv::View<W>;
    using Activation = ::ActivationExp<W>;

    static W sigma(W in) { return Activation::sigma(in); }
    static W sigma_der(W in)
    {
        return Activation::sigma_der(vvv::clamp_fast<W>(-20, 20, in));
    }

    Neuron(View weights, ConstView inputs, W& output, W& output_dot,
           W& input_error, View output_errors)
        : weights(weights), inputs(inputs), output(output),
          output_dot(output_dot), input_error(input_error),
          errors(output_errors)
    {
    }

    /// Change output value by input signals
    W feed()
    {
        output_dot = dot_bias(inputs, weights, bias);
        output = Neuron<W>::sigma(output_dot);
        return output;
    }

    /// Update weights by input signals and \p error
    /// Return resulting error
    /// k = sigmader(res) * learning_speed,
    ///    where res is an output of current neuron
    void update_weights(W learning_speed)
    {
        /// delta_i = input_error
        const W k = Neuron<W>::sigma_der(output_dot) * learning_speed;
        const W K = k * input_error;
        const auto s = weights.size();
        for (size_t i = 0; i < s; ++i)
            weights[i] += inputs[i] * K;
        bias += K;
    }

    void distribute_error()
    {
        const auto s = weights.size();
        const auto k = Neuron<W>::sigma_der(output_dot) * input_error;
        for (size_t i = 0; i < s; ++i)
            errors[i] += k * weights[i];
    }

private:
    View weights;
    ConstView inputs;
    W bias = 1.0;
    W& output;
    W& output_dot;
    W& input_error;
    View errors; ///< distribute error in weights proportion
};
