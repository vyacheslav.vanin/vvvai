#pragma once
#include "neuron.hpp"
#include <fstream>
#include <vector>
#include <vvvstdhelper/random.hpp>
#include <vvvstdhelper/view.hpp>

template <typename W = double>
class NNet {
public:
    using View = vvv::VarView<W>;
    using ConstView = vvv::View<W>;
    using LayerDesc = std::vector<size_t>;

    /// Create network by neurons count in each layer.
    /// minimal \p neurons_by_layer size 2
    /// first number is input layer, last number is output layer,
    /// other is hidden layers numbers
    NNet(const LayerDesc& neurons_by_layers);
    /// Create network with \p inputs and 1 output without hidden layers.
    /// Same as NNet(std::vector<size_t>{inputs, 1});
    NNet(size_t inputs);

    View getInputLayer() { return input_layer; }
    ConstView getOutputLayer() const { return output_layer; }

    ConstView feed(ConstView inputs);
    size_t classify(ConstView inputs);

    /// return view on outputs errors
    ConstView train(ConstView inputs, ConstView expected_outputs,
                    W learning_speed = 0.1);

    View getInputs() { return inputs; }
    View getWeights() { return weights; }
    ConstView getWeights() const { return weights; }
    View getErrors() { return errors; }
    ConstView getOutputErrors() const { return output_errors; }
    const LayerDesc& getLayerDesc() const { return neurons_by_layers; }

private:
    LayerDesc neurons_by_layers;
    std::vector<Neuron<W>> neurons;
    std::vector<W> inputs;
    std::vector<W> input_dots;
    std::vector<W> weights;
    std::vector<W> errors;
    View input_layer;
    View output_dots;
    View output_layer;
    View output_errors;
};

template <typename W>
void nn_write(std::ostream& str, const NNet<W>& nn);

template <typename W>
NNet<W> nn_read(std::istream& str);

template <typename W>
inline NNet<W> nn_read(const std::string& filename)
{
    std::ifstream f(filename);
    return nn_read<W>(f);
}
